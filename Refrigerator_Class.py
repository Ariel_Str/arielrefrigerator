from _datetime import  datetime
class Refrigerator:
    def __init__(self, id, type,color,listOfSelfs):
        self.numberOfShelfs = len(listOfSelfs)
        self.color = color
        self.id = id
        self.type = type
        self.listOfSelfs = listOfSelfs

    def __eq__(self, other):
        return self.type == other.type \
               and self.color == other.color \
               and self.listOfSelfs == other.listOfSelfs \
               and self.numberOfShelfs == other.numberOfShelfs

    def empty_space(self):
        sum = 0
        for shelf in self.listOfSelfs:
            sum += shelf.freeSpace
        return sum

    def add_shelf(self, shelf):
        self.listOfSelfs.append(shelf)

    def add_item_to_refrigerator(self, item):
        for shelf in self.listOfSelfs:
            if shelf.add_item_to_shelf(item):
                return True
        return False

    def remove_item_and_add(self, id):
        for shelf in self.listOfSelfs:
            for item in shelf.listOfItems:
                if item.id == id:
                    shelf.listOfItems.remove(item)
                    self.add_item_to_refrigerator(item)
                    return True
        return False

    def remove_expired(self):
        for shelf in self.listOfSelfs:
            for item in list(shelf.listOfItems):
                if(item.expiredDate < datetime.now()):
                    shelf.remove_item(item)

    def what_to_eat(self, kosher, type):
        listFood = []
        for shelf in self.listOfSelfs:
            for item in shelf.listOfItems:
                if item.kosher == kosher and item.type == type and item.expiredDate > datetime.now():
                    listFood.append(item.name)
        return listFood

    def is_time_for_shopping(self):
        listRemoved = []
        if not self.check_if_after_remove_there_is_place(self.remove_expired, listRemoved) \
            and not self.check_if_after_remove_there_is_place(self.remove_items_with_conditions, listRemoved, kosherType="dairy", expiredTime = 3) \
            and not self.check_if_after_remove_there_is_place(self.remove_items_with_conditions, listRemoved, kosherType="meat", expiredTime = 7) \
            and not self.check_if_after_remove_there_is_place(self.remove_items_with_conditions, listRemoved, kosherType="parve", expiredTime = 1):
                self.add_items(listRemoved)
                print("its not the time for shopping")
        else:
            if len(listRemoved) > 0 : print("All these items removed ", listRemoved)

    def check_if_after_remove_there_is_place(self, removeFunction, list, **Kwargv):
        if len(Kwargv) > 0:
            list += removeFunction(Kwargv["kosherType"], Kwargv["expiredTime"])
        else:
            removeFunction()
        if self.empty_space() > 20:
            print("There is enough space")
            return True
        return False

    def remove_items_with_conditions(self, kosherType, expiredTime):
        RemovesItems = []
        for shelf in self.listOfSelfs:
            for item in shelf.listOfItems:
                if item.kosher == kosherType and (item.expiredDate - datetime.now()).days < expiredTime:
                    shelf.remove_item(item)
                    RemovesItems.append(item)
        return  RemovesItems

    def add_items(self, listofitems):
        for item in listofitems:
            self.add_item_to_refrigerator(item)


    # def IsThere20Space(self):
    #     if self.emptySpace() > 20:
    #         print("There is enough space")
    #         return True
    #     return False
    #
    # def AvailbaleSpaceBeforeShopping(self):
    #     if self.IsThere20Space() == False:
    #         self.RemoveExpired()
    #         if self.IsThere20Space() == False:
    #             list = self.RemoveItemsWithConditions("dairy", 3)
    #             if self.IsThere20Space() == False:
    #                 list += self.RemoveItemsWithConditions("meat", 7)
    #                 if self.IsThere20Space() == False:
    #                     list += self.RemoveItemsWithConditions("parve",1)
    #             if self.emptySpace() > 20:
    #                 print("All these items removed " , list)
    #             else:
    #                 self.AddItems(list)
    #                 print("its not the time for shopping")















