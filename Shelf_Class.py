class Shelf:
    def __init__(self, id, floor, Space , listOfItems):
        self.floor = floor
        self.id = id
        self.Space = Space
        self.listOfItems = listOfItems
        self.update_free_space()

    def __eq__(self, other):
        return self.floor == other.floor \
               and self.Space == other.Space \
               and self.freeSpace == other.freeSpace \
               and self.listOfItems == other.listOfItems

    def add_item_to_shelf(self, item):
        if self.id == item.shelfId and self.freeSpace >= item.zone:
            self.listOfItems.append(item)
            self.freeSpace -= item.zone
            return True
        return False

    def update_free_space(self):
        sum = 0
        for item in self.listOfItems:
            sum += item.zone
        self.freeSpace = self.Space - sum

    def remove_item(self, item):
        self.listOfItems.remove(item)
        self.update_free_space()








