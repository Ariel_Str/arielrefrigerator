class Item:
    def __init__(self, id, shelfId, name, type, kosher, expiredDate, zone):
        self.zone = zone
        self.expiredDate = expiredDate
        self.kosher = kosher
        self.shelfId = shelfId
        self.type = type
        self.id = id
        self.name = name

    def __eq__(self, other):
        return self.zone == other.zone \
               and self.name == other.name \
               and self.shelfId == other.shelfId \
               and self.kosher == other.kosher \
               and self.type == other.type \
               and self.expiredDate == other.expiredDate

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)
