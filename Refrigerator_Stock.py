class RefrigeratorStock:
    def __init__(self):
        self.refrigerators = []

    def add_refrigerator(self, refrigerator):
        self.refrigerators.append(refrigerator)

    def sort_by_empty_space(self):
        self.refrigerators.sort(reverse=True, key= lambda a: a.empty_space())
        return self.refrigerators

